 import FormType from "../constants/FormType";
import {useEffect, useState} from "react";
import {formData} from "../data/formData";
 import {useNavigate , useLocation} from 'react-router-dom';


export const EQUAL_SIGN = "~";
export const AND_SIGN = "+";
export const ARRAY_SEPARATOR = "--";


// ! You can use this utils
function parseUrl(url) {
  return url
    ?.replace("?", "")
    ?.split(AND_SIGN)
    ?.filter((v) => v)
    ?.reduce((acc, cur) => {
      const [key, value] = cur.split(EQUAL_SIGN);
      return { ...acc, [key]:value.includes(ARRAY_SEPARATOR)?decodeURIComponent(value).split(ARRAY_SEPARATOR).map(item=>Number(item)): decodeURIComponent(value) };
    }, {});
}

function stringifyUrl(data) {
  if (data) {
    return Object.entries(data)
      ?.filter(([k, v]) => k && v)
      ?.reduce((acc, [key, val], idx) => {
        return `${acc}${
          idx === 0 ? "" : AND_SIGN
        }${key}${EQUAL_SIGN}${Array.isArray(val)? encodeURIComponent(val.join(ARRAY_SEPARATOR) ) : encodeURIComponent(val)}`;
      }, "?");
  }
}

function useFilter() {
    const InitialFilterState={}

    const [filterState,setState]=useState(InitialFilterState);

      const navigate = useNavigate();
      const location = useLocation();
     useEffect(()=>{
         // update url params  with filterState
         if (filterState!==InitialFilterState){
              let Filter=stringifyUrl(filterState);
              navigate(`/${Filter}`)
         }
    },[filterState])


    useEffect(()=>{
        // update filterState with url params

        let initialFilter=parseUrl(location.search)
        setState(initialFilter)

    },[])


  function onChange(e, name, type) {


      const setGroupCheckbox=(prev,next)=>{
          // ****if value inside in  previous state remove them and if is not add them
          if (prev.includes(Number(next))){
              return prev.filter(item=> item !==Number(next));
          }else {
              return [...prev,Number(next)]
          }
      }

        // *******Separate process between add in group Checkbox , checkbox  and others
      switch(type){
          case FormType.CHECKBOX_GROUP:
              return  setState(prev=>({...prev,[name]:setGroupCheckbox(filterState[name]?filterState[name]:[] ,e.target.value)}));
          case FormType.CHECKBOX:
              return Object.keys(filterState).includes(name)? onClear(name):setState(prev=>({...prev,[name]:e.target.value}));
           default:
              return setState(prev=>({...prev,[name]:e.target.value}));
          break
      }


  }

  function onClear(name) {
         console.log("clear")

      // delete main Type
      delete filterState[name];
      // delete child Type
      let childrenArray=Object.values(formData).filter(item=>item.name===name)[0]?.children
      if (childrenArray){
          childrenArray.map(childrenArray=>delete filterState[childrenArray])
      }
      // update State

      setState({...filterState});
  }
  function onClearAll() {
      setState({})
  }
  return { filterState, setState, onChange, onClear, onClearAll };
}

export default useFilter;
